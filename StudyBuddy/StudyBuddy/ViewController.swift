//
//  ViewController.swift
//  StudyBuddy
//
//  Created by Milo Gilad on 10/3/18.
//  Copyright © 2018 NBPS. All rights reserved.
//

import UIKit

struct User {
    static var number: String = "";
    static var name: String = "";
    static var grade: Int = 0;
    static var school: Int = 0;
}

extension UIButton {
    func makeRound() {
        self.layer.cornerRadius = 10;
        self.clipsToBounds = true;
    }
}

class ViewController: UIViewController {

    @IBOutlet weak var phoneNumber: UITextField!
    @IBOutlet weak var goButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        goButton.makeRound();
        phoneNumber.keyboardType = UIKeyboardType.numberPad;
    }

    @IBAction func goButton(_ sender: UIButton) {
        User.number = phoneNumber.text!;
    }
    
}

