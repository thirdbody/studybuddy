//
//  ContactUserViewController.swift
//  StudyBuddy
//
//  Created by Milo Gilad on 10/14/18.
//  Copyright © 2018 NBPS. All rights reserved.
//

import UIKit

class ContactUserViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    func sendToApp(_ data: String) {
        if let url = URL(string: data) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url);
            } else {
                UIApplication.shared.openURL(url);
            }
        }
    }
    
    @IBAction func sendEmail(_ sender: Any) {
        sendToApp("mailto:milo.gilad@mynbps.org")
    }
    
    @IBAction func sendSms(_ sender: Any) {
        sendToApp("sms:5617776339")
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
