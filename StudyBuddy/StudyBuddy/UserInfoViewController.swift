//
//  UserInfoViewController.swift
//  StudyBuddy
//
//  Created by Milo Gilad on 10/4/18.
//  Copyright © 2018 NBPS. All rights reserved.
//

import UIKit

class UserInfoViewController: UIViewController {
    
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var fullNameField: UITextField!
    @IBOutlet weak var schoolCodeField: UITextField!
    @IBOutlet weak var gradeLevelField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        schoolCodeField.keyboardType = UIKeyboardType.numberPad;
        gradeLevelField.keyboardType = UIKeyboardType.numberPad;
        
        nextButton.makeRound();
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    @IBAction func storeFieldInfo(_ sender: UIButton) {
        User.name = fullNameField.text!;
        User.school = Int(schoolCodeField.text!)!;
        User.grade = Int(gradeLevelField.text!)!;
    }
    
    
}
