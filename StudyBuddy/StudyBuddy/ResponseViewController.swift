//
//  ResponseViewController.swift
//  StudyBuddy
//
//  Created by Milo Gilad on 10/16/18.
//  Copyright © 2018 NBPS. All rights reserved.
//

import UIKit

class ResponseViewController: UIViewController {

    @IBOutlet weak var yesButton: UIButton!
    @IBOutlet weak var customRespButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        yesButton.makeRound();
        customRespButton.makeRound();
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
