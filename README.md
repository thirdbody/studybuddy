# PeerStudy

PeerStudy is an app that lets you study with your classmates.

Many people prefer in-class or friend-provided help over tutoring services. PeerStudy wants to help make that easier.